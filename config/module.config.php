<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 2/27/14
 * Time: 3:04 PM
 */
return array(
    'meta_collector'=>array(
        'enabled'=>true,
        'title_set_or_append'=>'append',
    ),
    'service_manager'=>array(
        'invokables'=>array(
            'MetaCollector'=>'MetaCollector\Collector\MetaCollector',
            'ModuleControllerActionFilter'=>'ClickLogical\Filter\ModuleControllerActionObject',
        ),
    ),
);