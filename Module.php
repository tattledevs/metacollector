<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 2/27/14
 * Time: 3:46 PM
 */

namespace MetaCollector;
use Zend\Mvc\MvcEvent;

class Module {

    /**
     * Bootstrap Event
     * @param MvcEvent $e
     */
    public function onBootstrap(MvcEvent $e)
    {
        $e->getApplication()->getEventManager()->attach(MvcEvent::EVENT_RENDER, array($this,'collectAction'));
    }

    /**
     * Collects all the meta data
     * @param MvcEvent $e
     */
    public function collectAction(MvcEvent $e)
    {
        $collector = $e->getApplication()->getServiceManager()->get('MetaCollector');
        $collector->setEvent($e);
        $collector->collect();
    }

    /**
     * Gets the module config
     * @return array
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Gets the autoloader config.
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
} 