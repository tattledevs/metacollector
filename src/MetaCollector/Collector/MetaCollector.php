<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 12/5/13
 * Time: 10:10 AM
 */

namespace MetaCollector\Collector;
use Filter\ModuleControllerActionObject;
use FileUtilities\File\File;
use Zend\Config\Reader\Ini;
use Zend\EventManager\EventInterface as Event;
use Zend\Mvc\InjectApplicationEventInterface;

class MetaCollector implements InjectApplicationEventInterface {

    /**
     * @var Event
     */
    protected $event;

    /**
     * Compose an Event
     *
     * @param  Event $event
     * @return void
     */
    public function setEvent(Event $event)
    {
        $this->event = $event;
    }

    /**
     * Retrieve the composed event
     *
     * @return Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Sets the Meta Data
     * @throws \Zend\Config\Exception\RuntimeException
     * @throws \Exception
     */
    public function collect()
    {
        $e = $this->getEvent();

        $config = $e->getApplication()->getServiceManager()->get('config');

        if( array_key_exists('meta_collector', $config))
        {
            $config = $config['meta_collector'];

            if( $config['enabled'] == false )
            {
                return;
            }
        }

        $route = $e->getRouteMatch();

        if( $route == null )
        {
            return;
        }

        $filter = $this->getModuleControllerActionFilter();
        $obj = $filter->filter($route->getParams());

        $globalMetaPath = getcwd() . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'meta' . DIRECTORY_SEPARATOR . 'default.meta';

        $controllerMetaPath = getcwd() . DIRECTORY_SEPARATOR . 'module' . DIRECTORY_SEPARATOR . $obj->module . DIRECTORY_SEPARATOR . 'meta' . DIRECTORY_SEPARATOR . $obj->controller . '.meta';
        $actionMetaPath = getcwd() . DIRECTORY_SEPARATOR . 'module' . DIRECTORY_SEPARATOR . $obj->module . DIRECTORY_SEPARATOR . 'meta' . DIRECTORY_SEPARATOR . $obj->controller . DIRECTORY_SEPARATOR . $obj->action . '.meta';

        $globalMetaFile = new File($globalMetaPath);
        $controllerMetaFile = new File($controllerMetaPath);
        $actionMetaFile = new File($actionMetaPath);

        $globalMeta = array();
        $actionMeta = array();
        $controllerMeta = array();

        if( $globalMetaFile->exists() )
        {
            $ini = new Ini();
            $globalMeta = $ini->fromFile($globalMetaFile->getRealPath());

            if( ! is_array($globalMeta) )
            {
                $globalMeta = array();
            }
        }

        if( $controllerMetaFile->exists() )
        {
            $ini = new Ini();
            $controllerMeta = $ini->fromFile($controllerMetaFile->getRealPath());

            if( ! is_array($controllerMeta) )
            {
                $controllerMeta = array();
            }
        }

        if( $actionMetaFile->exists() )
        {
            $ini = new Ini();
            $actionMeta = $ini->fromFile($actionMetaFile->getRealPath());

            if( ! is_array($actionMeta) )
            {
                $actionMeta = array();
            }
        }

        $meta = array_merge($globalMeta, $controllerMeta);
        $meta = array_merge($meta, $actionMeta);

        $viewHelperManager = $e->getApplication()->getServiceManager()->get('viewhelpermanager');

        if( array_key_exists('title', $meta))
        {
            if( isset($config['title_set_or_append']) && $config['title_set_or_append']== 'append')
            {
                $viewHelperManager->get('headTitle')->append($meta['title']);
            }
            else
            {
                $viewHelperManager->get('headTitle')->set($meta['title']);
            }
        }

        if( array_key_exists('meta', $meta))
        {
            foreach($meta['meta'] as $key => $value)
            {
                $viewHelperManager->get('headMeta')->appendName($key, $value);
            }
        }
    }

    /**
     * @return ModuleControllerActionObject
     */
    protected function getModuleControllerActionFilter()
    {
        return new ModuleControllerActionObject();
    }
} 